const Config = require("./config")
const clc = require("cli-color")
const onoff = require("onoff")
const Gpio = require("onoff").Gpio
const led = new Gpio(Config.gpio.led, "out")
const button = new Gpio(Config.gpio.button, "in", "both")

var mode = true

button.watch((err, value) => {
	console.log('button pressed', value)

	if (value == 1) {
	led.write(Config.led.on, () => {
	   console.log('led', clc.white('ON'))
	})
	} else {
	led.write(Config.led.off, () => {
	   console.log('led', clc.green('OFF'))
})
}
})
